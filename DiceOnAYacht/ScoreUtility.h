#pragma once
#include<unordered_map>
#include<iostream>
#include<string>
enum Category
{
	Ones = 1, Twos, Threes, Fours, Fives, Sixes,
	Sevens,	Eights,	ThreeOfAKind, FourOfAKind, FullHouse, 
	SmallStraight, LargeStraight, AllDifferent, Chance, AllSame
};

#define NUM_CATEGORIES 16
#define FULL_HOUSE_WIN 25
#define SMALL_STRAIGHT_WIN 30
#define LARGE_STRAIGHT_WIN 40
#define ALL_DIFFERENT_WIN 40
#define CHANCE_WIN 20
#define ALL_SAME_WIN 50

#define ALL_SAME_CHECK 5
#define TWO_CHECK 2
#define THREE_CHECK 3
#define FOUR_CHECK 4

using namespace std;
class ScoreUtility
{

private:
	vector<int>::const_iterator  dieValueIterator;
	struct scoreBool
	{
		bool isPresent;
		int	score;
		scoreBool() {}
		scoreBool(int win,bool present) :isPresent(present), score(win) {}
	};
	std::unordered_map<Category, scoreBool> categoryHash;

	int _straightValue;
	int _prevDieValue;
	int _prevtwoOfAKindNum;
	Category _bestCategory;

public:
	ScoreUtility()
	{
		wipeScores();
	}
	~ScoreUtility() {}

	//Score Update
	void update(const vector<int>& i_dieVector);

	//Getters
	string getCategoryName(const Category i_cat);

	Category getBestCategory()
	{
		return _bestCategory;
	}

	string getSuggestion(const vector<int>& dieRolls);
	string getSuggestion()
	{
		return getCategoryName(_bestCategory);
	}

	int getScore(const Category type, const vector<int>& i_dieVector);
	inline int getScore(const Category type)
	{
		return ((categoryHash[type].isPresent) ? categoryHash[type].score : 0);
	}
	

private:
	void wipeScores();

	//TwoOfAKind
	inline void checkTwoOfAKind(const int valueIndex,int& twoOfAKindNum)
	{
		if (categoryHash[static_cast<Category>(valueIndex)].score / valueIndex == TWO_CHECK)
		{
			if (twoOfAKindNum != 0)
				_prevtwoOfAKindNum = twoOfAKindNum;
			twoOfAKindNum = valueIndex;
		}
	}

	//FullHouse
	inline bool checkFullHouse(const int twoOfAKindNum,const int threeofAKindNum)
	{
		if (_prevtwoOfAKindNum != 0 && (twoOfAKindNum != threeofAKindNum 
			|| twoOfAKindNum != _prevtwoOfAKindNum))
		{
			return true;
		}
		return false;
	}

	//3 Of A Kind
	inline bool checkThreeOfAKind(const int valueIndex, int&threeofAKindNum)
	{
		if (categoryHash[static_cast<Category>(valueIndex)].score /valueIndex == THREE_CHECK)
		{
			threeofAKindNum = valueIndex;
			_bestCategory = ThreeOfAKind;
			return true;
		}
		return false;
	}

	//4 Kind
	inline bool checkFourOfAKind(const int valueIndex) 
	{
		if (categoryHash[static_cast<Category>(valueIndex)].score /valueIndex == FOUR_CHECK)
		{
			_bestCategory = FourOfAKind;
			return true;
		}
		return false;
	}

	//SmallStraight
	inline bool checkForSmallStraight()
	{
		if (_straightValue == 4)
		{
			_bestCategory = SmallStraight;
			return true;
		}
		return false;
	}

	//LargeStraight
	inline bool checkForLargeStraight()
	{
		if (_straightValue == 5)
		{
			_bestCategory = LargeStraight;
			return true;
		}
		return false;
	}

	//All Different
	inline bool checkAllDifferent(const int valueIndex)
	{
		if (categoryHash[static_cast<Category>(valueIndex)].score /valueIndex <= 1)
			return true;
		else
			return false;
	}

	// All Same
	inline bool checkAllSame(const int valueIndex)
	{
		if (categoryHash[static_cast<Category>(valueIndex)].score /valueIndex == ALL_SAME_CHECK)
		{
			_bestCategory = AllSame;
			return true;
		}
		return false;
	}
};
