#pragma once
#include "EightSideDice.h"
#include "ScoreUtility.h"
#include <list>
#include <vector>
#define NUMBER_OF_DICE 5
using namespace std;

class Game 
{
private:
	ScoreUtility* _scoreUtility;
	std::list<EightSideDice> _fiveOfEightDie;
	std::list<EightSideDice>::iterator _dieIterator;
	std::vector<int> _dieList;

	Game(int numberOfDie) :	_fiveOfEightDie(numberOfDie),
							_scoreUtility(new ScoreUtility()){}

	~Game() 
	{
		if (_scoreUtility)
			delete _scoreUtility;
	}

	void notify()
	{
		_scoreUtility->update(_dieList);
	}

	inline void displayInstructions()
	{
		std::cout << "* Press R to Roll or ESC to Exit." << std::endl;
		std::cout << "* After rolling: G to find the best suggestion on your roll." << endl;
		std::cout <<"* You can also cycle through the categories using [ or ] ." << endl;
	}

public:
	//Game Instance
	static Game* getInstancePointer()
	{
		static Game instance(NUMBER_OF_DICE); // 5 Dice
		return &instance;
	}

	//Gameplay functions
	void update();
	void rollDie();
};
