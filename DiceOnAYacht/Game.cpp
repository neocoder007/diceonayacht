#include "Game.h"
#include <iostream>
#include<conio.h>
#include<string>

#define ASCII_R 82
#define ASCII_PREV 91
#define ASCII_NEXT 93
#define ASCII_ENTER 13
#define ASCII_ESC 27
#define ASCII_G 71
#define ASCII_I 73

void Game::update()
{
	char c;
	int scoreSelect = 1;
	std::cout << "!!! Game Started !!!"<<endl;
	displayInstructions();

	while (true)
	{
		c = _getch();
		c = toupper(c);
		
		if (c == ASCII_ESC)//Exit
		{
			break;
		}
		else if (c == ASCII_R)// Roll
		{
			std::cout << " Rolling..." << endl;
			rollDie();
			std::cout << "..turn completed.(I for instructions)" << endl;
		}
		else if (c == ASCII_PREV)//Select Category		
		{
			if (scoreSelect != Ones)
				--scoreSelect;

			cout << "->"<<_scoreUtility->getCategoryName(static_cast<Category>(scoreSelect))
				<< " enter to display"<< endl;
		}
		else if (c == ASCII_NEXT)		
		{
			if (scoreSelect != AllSame)
				scoreSelect += Ones;

			cout << "->" << _scoreUtility->getCategoryName(static_cast<Category>(scoreSelect))
				<< " enter to display" << endl;
		}
		else if (c == ASCII_ENTER) //Get Current Category Score
		{
			std::cout << _scoreUtility->getCategoryName(static_cast<Category>(scoreSelect)) << "Score :"
				<< _scoreUtility->getScore (static_cast<Category>(scoreSelect))<< endl;
		}
		else if (c == ASCII_G)//Suggestion
		{
			cout << "Suggestion: " << _scoreUtility->getSuggestion() <<"! Score:"<< _scoreUtility->getScore(_scoreUtility->getBestCategory()) <<endl;
		}
		else if (c == ASCII_I)//Instruction
		{
			displayInstructions();
		}
		else 
		{	//Exception Input
			std::cout << "Invalid Input, press I for Instructions" << endl;
		}
	}
}

void Game:: rollDie()
{
	_dieList.clear();
	cout << "Die rolled: " ;
	for (_dieIterator = _fiveOfEightDie.begin(); _dieIterator != _fiveOfEightDie.end(); _dieIterator++)
	{
		int dieValue = (_dieIterator)->roll();
		cout << dieValue << ",";
		_dieList.push_back(dieValue);
	}
	cout << endl;
	//Notify Relavent listeners
	notify();
}