#include "Root.h"
using namespace Engine;

Root::Root()
{
	_game = NULL;
}

Root::~Root()
{}

void Root::init()
{
	_game = Game::getInstancePointer();
}

void Root::run()
{
	if(_game)
		_game->update();
}

