#pragma once
#include "Game.h"
namespace Engine
{
	class Root
	{
	public:
		Root();
		~Root();

		void init();
		void run();
	private:
		Game* _game;
	};
}