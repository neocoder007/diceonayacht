#include"ScoreUtility.h"
#include <iostream>

//Reset Vars
void ScoreUtility::wipeScores()
{
	_straightValue = _prevDieValue = _prevtwoOfAKindNum = 0;
	_bestCategory = Chance;

	categoryHash.clear();
	for (int i = Ones; i <= AllSame; i++)
	{
		int winAmount;
		int present = false;
		switch(i)
		{
			case FullHouse: 
				winAmount = FULL_HOUSE_WIN;
				break;
			case SmallStraight:
				winAmount = SMALL_STRAIGHT_WIN;
				break;
			case LargeStraight:
				winAmount = LARGE_STRAIGHT_WIN;
				break;
			case AllDifferent:
				winAmount = ALL_DIFFERENT_WIN;
				present = true;
				break;
			case Chance:
				winAmount = CHANCE_WIN;
				present = true;
				break;
			case AllSame:
				winAmount = ALL_SAME_WIN;
				break;
			default: 
				winAmount = 0;
				break;
		}
		scoreBool tempBool(winAmount, present);
		categoryHash[static_cast<Category>(i)] = tempBool;
	}
}

//UPDATE
void ScoreUtility::update(const vector<int>& i_dieVector)
{
	wipeScores();
	int dieVal = 0;
	int sumOfDie = 0;
	int twoOfAKindNum = 0;
	int threeofAKindNum = 0;

	dieValueIterator = i_dieVector.begin();

	//For SmallStraight
	_prevDieValue = *dieValueIterator - 1;

	//Loop thru the 5 die values
	for (; dieValueIterator != i_dieVector.end(); dieValueIterator++)
	{
		dieVal = *dieValueIterator;
		sumOfDie += dieVal;
		_straightValue += (dieVal - _prevDieValue == 1) ? 1 : 0;
		_prevDieValue = dieVal;

		//Unit Scores
		categoryHash[static_cast<Category>(dieVal)].isPresent = true;
		categoryHash[static_cast<Category>(dieVal)].score += dieVal;

		//Special Categories
		checkTwoOfAKind(dieVal,twoOfAKindNum);

		if(!categoryHash[ThreeOfAKind].isPresent)
			categoryHash[ThreeOfAKind].isPresent = (checkThreeOfAKind(dieVal,threeofAKindNum));

		if (!categoryHash[FourOfAKind].isPresent)
			categoryHash[FourOfAKind].isPresent = (checkFourOfAKind(dieVal));

		if(categoryHash[AllDifferent].isPresent)
			categoryHash[AllDifferent].isPresent = (checkAllDifferent(dieVal));

		categoryHash[AllSame].isPresent = (checkAllSame(dieVal));
		
		categoryHash[SmallStraight].isPresent = checkForSmallStraight();
		categoryHash[LargeStraight].isPresent = checkForLargeStraight();

	}

	//Chance Always present if die have been rolled
	categoryHash[Chance].score = sumOfDie;

	//Of a Kind/ Full House
	if (categoryHash[FourOfAKind].isPresent)
	{
		categoryHash[FourOfAKind].score = sumOfDie;
	}
	else if (categoryHash[ThreeOfAKind].isPresent)
	{
		categoryHash[ThreeOfAKind].score = sumOfDie;

		//Full House
		categoryHash[FullHouse].isPresent = checkFullHouse(twoOfAKindNum, threeofAKindNum);
	}

	//Set Best Category for Exceptions
	if (categoryHash[FullHouse].isPresent && sumOfDie < FULL_HOUSE_WIN)
		_bestCategory = FullHouse;

	if (categoryHash[AllDifferent].isPresent && _bestCategory != AllSame)
		_bestCategory = AllDifferent;
}


string ScoreUtility::getSuggestion(const vector<int>& i_dieVector)
{
	update(i_dieVector);
	return getSuggestion();
}

int ScoreUtility::getScore(Category type, const vector<int>& i_dieVector)
{
	update(i_dieVector);
	return getScore(type);
}

string ScoreUtility::getCategoryName(const Category i_cat)
{
	switch (i_cat)
	{
	case Ones:
		return "Ones";
	case Twos:
		return "Twos";
	case Threes:
		return "Threes";
	case Fours:
		return "Fours";
	case Fives:
		return "Fives";
	case Sixes:
		return "Sixes";
	case Sevens:
		return "Sevens";
	case Eights:
		return "Eights";
	case ThreeOfAKind:
		return "ThreeOfAKind";
	case FourOfAKind:
		return "FourOfAKind";
	case FullHouse:
		return "FullHouse";
	case SmallStraight:
		return "SmallStraight";
	case LargeStraight:
		return "LargeStraight";
	case AllDifferent:
		return "AllDifferent";
	case Chance:
		return "Chance";
	case AllSame:
		return "AllSame";
	default:
		return "Invalid Category";
	}
}