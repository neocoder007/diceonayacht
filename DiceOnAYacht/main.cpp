#include "Root.h"
using namespace Engine;

void main()
{
	Root* root = new Root();
	root->init();
	root->run();
	delete root;
}