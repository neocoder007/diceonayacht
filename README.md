#Readme

Code:

I have written a small game based on document of DiceOnAYacht. It has all the scoring categories mentioned. The program itself is written in C++. I have followed a very simple game/engine style architecture, where 'Root' class drives the 'Game'. As there can be only one instance of a game , I decided to follow a singleton approach and have the 'ScoringUtilty' class listening to it after every roll. I went with this approach to make all categories of scores available after every roll instead of waiting to calculate them at a later point. I have used 'EightSidedDie' class with time as a unique seed for randomizing numbers from 1-8 for each of the 5 die. 

All this is written in such a way that any of the components can be switched with very minimum modifications to the core game. For example, a mini bonus round could be spawned if all the die are same by adding a new bonus class and adding it after every roll, or we could switch the 8 sided die for 6 sided to name few.

Game:

I have built an exe and attached it in the mail as well as here in the Release folder. It runs a simple console based game and should have straight forward instructions on how to go about the game. R- Roll, G- Best Suggestion, Esc - Exit , [ or ] to scroll individual categories.

Note: 

There are a couple of additional getter functions written to accomodate all the requirements of the test but might not be accessible through the console game. For example getSuggestion(const vector<int>& dieRolls)(similar to pseudo code) is not used as we already have the dieRolls after rolling. So, the game uses the variation of overloaded getSuggestion() for getting the best suggestion based on the current dieRolls after every round automatically. I am sure it will be self explanatory once you see the code.